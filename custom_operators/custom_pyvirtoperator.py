import json
import logging

from airflow.operators.python_operator import PythonVirtualenvOperator
from airflow.utils.decorators import apply_defaults


class CustomPyVirtOperator(PythonVirtualenvOperator):

    @apply_defaults
    def __init__(self, pip_config_file, **kwargs):
        with open(pip_config_file) as pip_configuration:
            self.pip_config = json.load(pip_configuration)
        super(CustomPyVirtOperator, self).__init__(**kwargs)

    def _generate_pip_install_cmd(self, tmp_dir):
        if len(self.requirements) == 0:
            return []
        else:
            if self.pip_config is not None:
                logging.info("Using custom pip config")
                cmd = ['{}/bin/pip'.format(tmp_dir), 'install', "--index-url", self.pip_config['index-url']]
            else:
                logging.info("pip config not found. using system default pypi index")
                cmd = ['{}/bin/pip'.format(tmp_dir), 'install']
            return cmd + self.requirements
