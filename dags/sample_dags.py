import pathlib

from airflow import DAG
from airflow.hooks.postgres_hook import PostgresHook
from airflow.sensors.sql_sensor import SqlSensor
from airflow.utils.dates import days_ago

from custom_operators.custom_pyvirtoperator import CustomPyVirtOperator
from semind.virtual_module import virtcallable

current_dir = pathlib.Path(__file__).parent.absolute()
pip_config = current_dir.__str__() + "/../configs/pip_config.json"


def do_processing():
    pg_hook = PostgresHook(postgres_conn_id='postgres_default')
    print("entering processor")
    rs = pg_hook.get_records("select id from basetable where todo;")
    for r in rs:
        print("inserting ", r)
        pg_hook.run("insert into desttable(id) values (%s)", parameters=r)
        pg_hook.run("update basetable set todo = false where id=%s", parameters=r[0])


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'start_date': days_ago(1)
    # 'retries': 1,
    # 'retry_delay': timedelta(minutes=5),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}

dag = DAG(
    'autodetect-db-change',
    default_args=default_args,
    description='A dag that detect a change in db and executes a python callable',
    schedule_interval=None,
    max_active_runs=1
)

with dag:
    t1 = SqlSensor(
        task_id="input_sensor",
        conn_id='postgres_default',
        sql="select id from basetable where todo;",
        poke_interval=10
    )

    # t1 = FileSensor(
    #   task_id="file_sensor_task",
    #   filepath="test_file.txt",
    #   fs_conn_id="local_filesystem"
    # )

    # t2 = PythonVirtualenvOperator(task_id="processor", python_callable=do_processing)

    t3 = CustomPyVirtOperator(task_id="virtualcall", python_callable=virtcallable, requirements=["wikipedia"],
                              pip_config_file=pip_config)

    t1 >> t3
